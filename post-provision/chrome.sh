#!/bin/bash

# Chrome.
wget https://s3.amazonaws.com/circle-downloads/google-chrome-stable_current_amd64_47.0.2526.73-1.deb
sudo dpkg -i google-chrome-stable_current_amd64_47.0.2526.73-1.deb
sudo apt-get update -y
sudo apt-get install -f -y

# Chrome Driver.
wget https://chromedriver.storage.googleapis.com/2.21/chromedriver_linux64.zip
sudo unzip -uq -d /usr/local/bin chromedriver_linux64.zip
sudo chmod a+r,a+x /usr/local/bin/chromedriver
